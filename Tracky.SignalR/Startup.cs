﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

using Newtonsoft.Json;

using Tracky.BL.Managers;
using Tracky.DAL;
using Tracky.DAL.Repository;
using Tracky.Model;

namespace Tracky.SignalR {

    public class Startup {

        public static IServiceProvider __serviceProvider;

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services) {
            services.AddSignalR(options => {
                options.Hubs.EnableDetailedErrors = true;
                options.Hubs.EnableJavaScriptProxies = true;
                options.EnableJSONP = true;
            });

            services.AddCors();

            // Add framework services.
            services.AddDbContext<DatabaseContext>(options =>
                options.UseSqlServer(DatabaseConstants.DatabaseUrl));

            services.AddScoped(typeof(IUserRepository), typeof(UserRepository));
            services.AddTransient<IUserManager, UserManager>();

            services.AddScoped(typeof(IEventRepository), typeof(EventRepository));
            services.AddTransient<IEventManager, EventManager>();

            //Authentication
            services.AddIdentity<User, UserRole>(options => {
                    //User setting
                    options.User.RequireUniqueEmail = true;

                    // Password settings
                    options.Password.RequireDigit = false;
                    options.Password.RequiredLength = 8;
                    options.Password.RequireNonAlphanumeric = false;
                    options.Password.RequireUppercase = false;
                    options.Password.RequireLowercase = false;

                    // Lockout settings
                    options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(30);
                    options.Lockout.MaxFailedAccessAttempts = 10;

                    // Cookie settings
                    options.Cookies.ApplicationCookie.ExpireTimeSpan = TimeSpan.FromDays(150);
                    options.Cookies.ApplicationCookie.LoginPath = "/users/LogIn";
                    options.Cookies.ApplicationCookie.LogoutPath = "/users/LogOut";
                })
                .AddEntityFrameworkStores<DatabaseContext, int>()
                .AddDefaultTokenProviders();

            __serviceProvider = services.BuildServiceProvider();

            JsonConvert.DefaultSettings = () => new JsonSerializerSettings {
                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            };
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory) {
            loggerFactory.AddConsole();

            if (env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
            }

            //https://elanderson.net/2016/11/cross-origin-resource-sharing-cors-in-asp-net-core/
            app.UseCors(builder => {
                    builder.AllowAnyHeader();
                    builder.AllowAnyMethod();
                    builder.AllowAnyOrigin();
                }
            );


            app.UseIdentity();
            app.UseDefaultFiles();
            app.UseStaticFiles();
            app.UseWebSockets();
            app.UseSignalR();
        }

    }

}