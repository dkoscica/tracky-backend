﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.WebSockets;
using System.Threading.Tasks;

using Microsoft.AspNetCore.SignalR;
using Microsoft.AspNetCore.SignalR.Messaging;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

using Newtonsoft.Json;

using Tracky.BL.Managers;

namespace Tracky.SignalR {

    /**
     * Client side methods to be invoked by UserEventHub
     */
    public interface IUserEventHub {

        Task OnConnected(string connectionId);
        Task OnDisconnected(string connectionId);
        void ReceiveEventUpdates(string json);
        void ReceiveUserUpdates(string json);

    }

    public class UserEventHub : Hub<IUserEventHub> {

        private readonly IEventManager _eventManager;
        private readonly IUserManager _userManager;

        public UserEventHub() {
            _eventManager = (IEventManager) Startup.__serviceProvider.GetRequiredService(typeof(IEventManager));
            _userManager = (IUserManager) Startup.__serviceProvider.GetRequiredService(typeof(IUserManager));
        }

        public override Task OnConnected() {
            var message = $"User with Id: {Context.ConnectionId} connected to {this.GetType().Name}";
            return Clients.Client(Context.ConnectionId).OnConnected(message);
        }

        public override Task OnDisconnected(bool stopCalled) {
            var message = $"User with Id: {Context.ConnectionId} disconnected from {this.GetType().Name}";
            return Clients.All.OnDisconnected(message);
        }

        /**
         * Server side methods called from client
         */
        public void PushNewEventUpdate() {
            var events = _eventManager.GetAllEventsAsync().Result;
            var json = JsonConvert.SerializeObject(events);
            this.Clients.All.ReceiveEventUpdates(json);
        }

        public void PushNewUserUpdate() {
            var users = _userManager.GetAllUsersAsync().Result;
            var json = JsonConvert.SerializeObject(users);
            this.Clients.All.ReceiveUserUpdates(json);
        }

    }

}