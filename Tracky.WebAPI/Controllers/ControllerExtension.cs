﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;

using Tracky.WebAPI.Responses;

namespace Tracky.WebAPI.Controllers {

    public static class ControllerExtension {

        public static IActionResult OkResult(this Controller controller, object data = null) {
            return new ApiResponseResult(new ApiResponseObject(ApiResponseStatus.Success, 200, data));
        }

        public static IActionResult NotFoundResult(this Controller controller, string message) {
            return new ApiResponseResult(new ApiResponseObject(ApiResponseStatus.Fail, 404, message: message));
        }

        public static IActionResult BadRequestResult(this Controller controller, string message) {
            return new ApiResponseResult(new ApiResponseObject(ApiResponseStatus.Fail, 400, message: message));
        }

        public static IActionResult BadRequestResult(this Controller controller, ModelStateDictionary modelState) {
            var errorMessages = (from item in modelState
                where item.Value.Errors.Any()
                select item.Value.Errors[0].ErrorMessage).ToList();

            var message = errorMessages.Aggregate((error, nextError) => $"{error} {nextError}");

            return new ApiResponseResult(new ApiResponseObject(ApiResponseStatus.Fail, 400, message: message));
        }

    }

}