﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNet.SignalR.Client;
using Microsoft.AspNetCore.Mvc;

namespace Tracky.WebAPI.Controllers {

    public abstract class ApiHubController : Controller {

        protected IHubProxy HubProxy;
        protected HubConnection HubConnection;

        protected ApiHubController(string hubConnectionUrl, string hubName) {
            HubConnection = new HubConnection(hubConnectionUrl);
            HubProxy = HubConnection.CreateHubProxy(hubName);
        }

        protected void InvokeHubMethodWithName(string methodName) {
            HubConnection.Start();
            HubProxy.On("OnConnected", data => {
                HubProxy.Invoke(methodName);

                HubProxy.On("receiveEventUpdates", json => {
                    Console.WriteLine("Incoming data: {0}", json);
                });
            });
        }

        /**
         *  HubProxy.On("OnConnected", data => {
                Console.WriteLine("Incoming data: {0}", data);
                HubProxy.Invoke(HubActionName);
            });
            HubProxy.On("receiveEventUpdates", data => {
                Console.WriteLine("Incoming data: {0}", data);
            });
         */

    }

}