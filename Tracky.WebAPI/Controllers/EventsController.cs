﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;

using Tracky.BL.Exceptions;
using Tracky.BL.Managers;
using Tracky.DAL;
using Tracky.DAL.Repository;
using Tracky.Model;
using Tracky.WebAPI.Filters;
using Tracky.WebAPI.ViewModels;

namespace Tracky.WebAPI.Controllers {

    [Route(ApiConstants.VersionPrefix + "[controller]")]
    public class EventsController : ApiHubController {

        private const string HubUrl = "http://dkoscica-001-site3.gtempurl.com/signalr";
        private const string HubName = "UserEventHub";
        private const string HubPushNewEventUpdate = "PushNewEventUpdate";
        private const string HubPushNewUserUpdate = "PushNewUserUpdate";

        private static string EventNotFoundMessage(int id) => $"Event with Id({id}) could not be found!";

        private readonly IEventManager _eventManager;
        private readonly IUserManager _userManager;
        private readonly ILogger _logger;

        public EventsController(IEventManager eventManager, IUserManager userManager,
            ILogger<UsersController> logger) : base(HubUrl, HubName) {
            _eventManager = eventManager;
            _userManager = userManager;
            _logger = logger;
        }

        [HttpGet]
        public async Task<IActionResult> Get() {
            var events = await _eventManager.GetAllEventsAsync();

            /**
            foreach (var _event in events) {
                foreach (var userEvent in _event.UserEvents) {
                    //TODO Ask Dobriša
                    var user = await _userManager.GetUserAsync(userEvent.UserId);
                    _event.Participants.Add(user);
                }
            }
    **/

            return this.OkResult(events);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id) {
            var _event = await _eventManager.GetEventWithAllParticipants(id);
            if (_event == null) {
                return this.NotFoundResult(EventNotFoundMessage(id));
            }

            foreach (var userEvent in _event.UserEvents) {
                var user = await _userManager.GetUserAsync(userEvent.UserId);
                _event.Participants.Add(user);
            }

            return this.OkResult(_event);
        }

        [HttpPost]
        [ValidateActionParameters]
        public async Task<IActionResult> Post([FromBody] [Required] Event _event) {
            if (_event == null || !ModelState.IsValid) {
                return this.BadRequestResult(ModelState);
            }

            try {
                await _eventManager.SaveEventAsync(_event);
                InvokeHubMethodWithName(HubPushNewEventUpdate);
                return CreatedAtAction("Get", new {id = _event.Id}, _event);
            } catch (UserException e) {
                return this.BadRequestResult(e.Message);
            }
        }

        [HttpPut("{id}")]
        [ValidateActionParameters]
        public async Task<IActionResult> Put(int id, [FromBody] [Required] Event _event) {
            if (_event == null || !ModelState.IsValid) {
                return this.BadRequestResult(ModelState);
            }

            var persistedEvent = await _eventManager.GetEventAsync(id);
            if (persistedEvent == null) {
                return this.NotFoundResult(EventNotFoundMessage(id));
            }

            persistedEvent.Update(_event);

            await _eventManager.UpdateEventAsync(persistedEvent);
            return this.OkResult(_event);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id) {
            var _event = await _eventManager.GetEventAsync(id);
            if (_event == null) {
                return this.NotFoundResult(EventNotFoundMessage(id));
            }
            await _eventManager.DeleteEventAsync(id);
            return this.OkResult(_event);
        }

        [HttpPost("addUserToEvent")]
        [ValidateActionParameters]
        public async Task<IActionResult> AddUserToEvent([FromBody] [Required] EventRegistrationViewModel viewModel) {
            if (viewModel == null || !ModelState.IsValid) {
                return this.BadRequestResult(ModelState);
            }

            var _event = await _eventManager.GetEventWithAllParticipants(viewModel.EventId);
            var user = await _userManager.GetUserAsync(viewModel.UserId);
            if (_event == null) {
                return this.NotFoundResult(EventNotFoundMessage(viewModel.EventId));
            }

            if (!_event.SecretCode.Equals(viewModel.SecretCode)) {
                return this.BadRequestResult("Wrong secret code for this event!");
            }

            try {
                await _eventManager.AddParticipantToEvent(user, _event);
            } catch (Exception e) {
                return this.BadRequestResult("User has already joined the event!");
            }

            var message =
                $"User with Id({viewModel.UserId}) successfully joined the Event with Id({viewModel.EventId})";
            InvokeHubMethodWithName(HubPushNewEventUpdate);
            InvokeHubMethodWithName(HubPushNewUserUpdate);

            return this.OkResult(message);
        }

    }

}