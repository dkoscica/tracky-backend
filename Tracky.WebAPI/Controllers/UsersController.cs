﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

using Microsoft.AspNet.SignalR.Client;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.Logging;

using ServiceReference1;

using Tracky.BL.Exceptions;
using Tracky.BL.Managers;
using Tracky.DAL;
using Tracky.DAL.Repository;
using Tracky.Model;
using Tracky.WebAPI.Filters;
using Tracky.WebAPI.Responses;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Tracky.WebAPI.Controllers {

    [Route(ApiConstants.VersionPrefix + "[controller]")]
    public class UsersController : Controller {

        private static string UserNotFoundMessage(int id) => $"User with Id({id}) could not be found!";

        private readonly IUserManager _userManager;
        private readonly ILogger _logger;

        public UsersController(IUserManager userManager, ILogger<UsersController> logger) {
            _userManager = userManager;
            _logger = logger;
        }

        [HttpPost]
        [Route("login")]
        public async Task<IActionResult> Login([FromBody] Credentials credentials) {
            var user = await _userManager.LoginUser(credentials.Email, credentials.Password);
            if (user == null) {
                return this.NotFoundResult($"User with Email({credentials.Email}) could not be found!");
            }
            return this.OkResult(user);
        }

        [HttpGet]
        public async Task<IActionResult> Get() {
            var users = await _userManager.GetAllUsersAsync();
            return this.OkResult(users);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id) {
            var user = await _userManager.GetUserAsync(id);
            if (user == null) {
                return this.NotFoundResult(UserNotFoundMessage(id));
            }
            return this.OkResult(user);
        }

        [HttpPost]
        [ValidateActionParameters]
        public async Task<IActionResult> Post([FromBody] [Required] User user) {
            if (user == null || !ModelState.IsValid) {
                return this.BadRequestResult(ModelState);
            }

            try {
                var savedUser = await _userManager.SaveUserAsync(user);
                return this.OkResult(savedUser);
            } catch (UserException e) {
                return this.BadRequestResult(e.Message);
            }
        }

        [HttpPut("{id}")]
        [ValidateActionParameters]
        public async Task<IActionResult> Put(int id, [FromBody] [Required] User user) {
            if (user == null || !ModelState.IsValid) {
                return this.BadRequestResult(ModelState);
            }

            var persistedUser = await _userManager.GetUserAsync(id);
            if (persistedUser == null) {
                return this.NotFoundResult(UserNotFoundMessage(id));
            }

            persistedUser.Update(user);

            var result = await _userManager.UpdateUserAsync(persistedUser);
            return this.OkResult();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id) {
            var user = await _userManager.GetUserAsync(id);
            if (user == null) {
                return this.NotFoundResult(UserNotFoundMessage(id));
            }
            await _userManager.DeleteUserAsync(user);
            return this.OkResult(user);
        }

    }

}