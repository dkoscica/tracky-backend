﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;

namespace Tracky.WebAPI.Responses {

    public enum ApiResponseStatus {

        Success,
        Fail,
        Error

    }

    public class ApiResponseObject {

        public ApiResponseStatus Status { get; }
        public int StatusCode { get; }
        public object Data { get; }
        public string Message { get; }

        private ApiResponseObject() { }

        public ApiResponseObject(ApiResponseStatus status, int statusCode, object data = null, string message = null) {
            this.Status = status;
            this.StatusCode = statusCode;
            this.Data = data;
            this.Message = message;
        }

    }

    public class ApiResponseResult : ObjectResult {

        public ApiResponseResult(ApiResponseObject apiResponseObject) : base(apiResponseObject) {
            this.Value = new {
                status = Enum.GetName(typeof(ApiResponseStatus), apiResponseObject.Status),
                statusCode = apiResponseObject.StatusCode,
                data = apiResponseObject.Data,
                message = apiResponseObject.Message
            };
        }

    }

}