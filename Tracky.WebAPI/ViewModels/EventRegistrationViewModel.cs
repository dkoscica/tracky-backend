﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Tracky.WebAPI.ViewModels {

    public class EventRegistrationViewModel {

        [Required]
        [Range(1, int.MaxValue,
            ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public int EventId { get; set; }

        [Required]
        [Range(1, int.MaxValue,
            ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public int UserId { get; set; }

        [Required]
        public string SecretCode { get; set; }

    }

}