﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

using Tracky.Model;

namespace Tracky.Model {

    public class Event : BaseEntity {

        [Required]
        [StringLength(30, MinimumLength = 3,
            ErrorMessage = "The Event name cannot be longer than {1} and shorter then {2} characters.")]
        [Display(Name = "Event name")]
        public string Name { get; set; }

        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Max number of participants must be between {1} and {2}.")]
        [Display(Name = "Max number of participants")]
        public int MaxNumberOfParticipants { get; set; }

        [Required]
        [StringLength(30, MinimumLength = 5,
            ErrorMessage = "The Secret cide cannot be longer than {1} and shorter then {2} characters.")]
        [Display(Name = "Secret code")]
        public string SecretCode { get; set; }

        [Required]
        [StringLength(30, MinimumLength = 3,
            ErrorMessage = "The Location name cannot be longer than {1} and shorter then {2} characters.")]
        [Display(Name = "Location name")]
        public string LocationName { get; set; }

        [Required]
        [StringLength(100, MinimumLength = 5,
            ErrorMessage = "The Address cannot be longer than {1} and shorter then {2} characters.")]
        public string Address { get; set; }

        [Required]
        [DisplayFormat(DataFormatString = "{0:#.######}", ApplyFormatInEditMode = true)]
        public decimal Latitude { get; set; }

        [Required]
        [DisplayFormat(DataFormatString = "{0:#.######}", ApplyFormatInEditMode = true)]
        public decimal Longitude { get; set; }

        [Required(ErrorMessage = "Start Date Required")]
        [DataType(DataType.DateTime, ErrorMessage = "Invalid Date Time Format")]
        [DisplayFormat(DataFormatString = DateTimeFormat, ApplyFormatInEditMode = true)]
        [Display(Name = "Start Date & Time")]
        public DateTime? StartDateTime { get; set; }

        [Required(ErrorMessage = "End Date Required")]
        [DataType(DataType.DateTime, ErrorMessage = "Invalid Date Time Format")]
        [DisplayFormat(DataFormatString = DateTimeFormat, ApplyFormatInEditMode = true)]
        [Display(Name = "End Date & Time")]
        public DateTime? EndDateTime { get; set; }

        [NotMapped]
        public string FormatedStartDateTime => StartDateTime != null ? string.Format(DateTimeFormat, StartDateTime) : null;

        [NotMapped]
        public string FormatedEndDateTime => EndDateTime != null
            ? string.Format(DateTimeFormat, EndDateTime)
            : null;

        public ICollection<UserEvent> UserEvents { get; set; } = new List<UserEvent>();

        public ICollection<User> Participants { get; set; } = new List<User>();

        public int RegisteredParticipantsCount => UserEvents?.Count ?? 0;

        public void Update(Event ev) {
            Name = ev.Name;
            MaxNumberOfParticipants = ev.MaxNumberOfParticipants;
            SecretCode = ev.SecretCode;
            LocationName = ev.LocationName;
            Address = ev.Address;
            Latitude = ev.Latitude;
            Longitude = ev.Longitude;
            StartDateTime = ev.StartDateTime;
            EndDateTime = ev.EndDateTime;
        }

        public string ToJson() {
            return Newtonsoft.Json.JsonConvert.SerializeObject(this);
        }
    }

}