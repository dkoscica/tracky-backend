﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Text;

using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

using Tracky.Model;

namespace Tracky.Model {

    public class UserRole : IdentityRole<int> {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public override int Id { get; set; }

    }

    public class User : IdentityUser<int> {

        private const string DateTimeFormat = "{0:d/M/yyyy HH:mm}";

        [Required]
        public override string UserName { get; set; }

        private string _email;

        [Required]
        [EmailAddress]
        public override string Email {
            get => _email;
            set {
                _email = value;
                UserName = Email;
            }
        }

        [Required]
        [StringLength(30, MinimumLength = 8)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [StringLength(30, MinimumLength = 3)]
        [Display(Name = "First name")]
        public string FirstName { get; set; }

        [Required]
        [StringLength(30, MinimumLength = 3)]
        [Display(Name = "Last name")]
        public string LastName { get; set; }

        [Required]
        [Display(Name = "Phone number")]
        [Phone]
        [DataType(DataType.PhoneNumber)]
        public override string PhoneNumber { get; set; }

        public ICollection<UserEvent> UserEvents { get; set; } = new List<UserEvent>();

        public DateTime? DateCreated { get; set; }

        public DateTime? DateModified { get; set; }

        [NotMapped]
        public string FormatedDateCreated => DateCreated != null ? String.Format(DateTimeFormat, DateCreated) : null;

        [NotMapped]
        public string FormatedDateModified => DateModified != null
            ? String.Format(DateTimeFormat, DateModified)
            : null;

        /**
         * Used to update the entity properties
         */
        public void Update(User user) {
            Email = user.Email;
            Password = user.Password;
            FirstName = user.FirstName;
            LastName = user.LastName;
            PhoneNumber = user.PhoneNumber;
        }
    }

}