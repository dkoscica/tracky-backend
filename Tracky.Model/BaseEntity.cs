﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Tracky.Model {

    public class BaseEntity {

        protected const string DateTimeFormat = "{0:dd/M/yyyy HH:mm}";

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        /*
        [Required(ErrorMessage = "Date Required")]
        [DataType(DataType.Date, ErrorMessage = "Invalid Date Format")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        */
        public DateTime? DateCreated { get; set; }

        public DateTime? DateModified { get; set; }

        [NotMapped]
        public string FormatedDateCreated => DateCreated != null ? string.Format(DateTimeFormat, DateCreated) : null;

        [NotMapped]
        public string FormatedDateModified => DateModified != null
            ? string.Format(DateTimeFormat, DateModified)
            : null;

    }

}