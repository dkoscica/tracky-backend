﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tracky.Model {

    public class Credentials {

        public string Email { get; set; }
        public string Password { get; set; }

    }

}