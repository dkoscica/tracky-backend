﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Tracky.Model;

namespace Tracky.Web.Models {

    public class CreateOrEditEventViewModel {

        public Event Event { get; set; }
        public List<User> RegisteredUsers { get; set; }

    }

}