using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

using Tracky.BL.Managers;
using Tracky.DAL;
using Tracky.DAL.Repository;
using Tracky.Model;
using Tracky.Web.Models;
using Tracky.Web.Controllers;

namespace Tracky.Web.Controllers {

    public class EventsController : ApiHubController {

        private const string HubUrl = "http://dkoscica-001-site3.gtempurl.com/signalr";
        private const string HubName = "UserEventHub";
        private const string HubPushNewEventUpdate = "PushNewEventUpdate";
        private const string HubPushNewUserUpdate = "PushNewUserUpdate";

        private readonly IEventManager _eventManager;
        private readonly IUserManager _userManager;

        public EventsController(IEventManager eventManager, IUserManager userManager) : base(HubUrl, HubName){
            _eventManager = eventManager;
            _userManager = userManager;
        }

        // GET: Students
        public async Task<IActionResult> Index(string sortOrder) {
            return View(await _eventManager.GetAllEventsAsync());
        }

        // GET: Students/Details/5
        public async Task<IActionResult> Details(int id) {
            var singleEvent = await _eventManager.GetEventWithAllParticipants(id);
            if (singleEvent == null) {
                return NotFound();
            }
            return View(singleEvent);
        }

        // GET: Students/Create
        public IActionResult Create() {
            return View();
        }

        // POST: Students/
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult>
            Create(CreateOrEditEventViewModel viewModel) {
            if (!ModelState.IsValid) {
                return View(viewModel);
            }

            viewModel.Event.DateCreated = DateTime.UtcNow;
            viewModel.Event.DateModified = DateTime.UtcNow;

            var errors = ModelState
                .Where(x => x.Value.Errors.Count > 0)
                .Select(x => new {x.Key, x.Value.Errors})
                .ToArray();

            if (ModelState.IsValid) {
                await _eventManager.SaveEventAsync(viewModel.Event);
                InvokeHubMethodWithName(HubPushNewEventUpdate);
                return RedirectToAction("Index");
            }
            return View(viewModel);
        }

        public async Task<IActionResult> AddUserToEvent(int eventId, int userId) {
            var _event = await _eventManager.GetEventWithAllParticipants(eventId);
            var user = await _userManager.GetUserAsync(userId);
            if (_event == null) {
                return NotFound();
            }

            await _eventManager.AddParticipantToEvent(user, _event);
            var newEvent = await _eventManager.GetEventWithAllParticipants(eventId);
            InvokeHubMethodWithName(HubPushNewUserUpdate);

            return RedirectToAction("Edit", new {id = eventId});
        }

        // GET: Students/Edit/5
        public async Task<IActionResult> Edit(int id) {
            var singleEvent = await _eventManager.GetEventWithAllParticipants(id);
            if (singleEvent == null) {
                return NotFound();
            }

            var participants = new List<User>();
            foreach (var userEvent in singleEvent.UserEvents) {
                var user = await _userManager.GetUserAsync(userEvent.UserId);
                participants.Add(user);
            }

            singleEvent.Participants = participants;
            var registeredUsers = await _userManager.GetAllUsersAsync();

            var viewModel = new CreateOrEditEventViewModel() {
                Event = singleEvent,
                RegisteredUsers = registeredUsers
            };

            return View(viewModel);
        }

        // POST: Students/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, CreateOrEditEventViewModel viewModel) {
            var singleEvent = viewModel.Event;
            if (id != singleEvent.Id) {
                return NotFound();
            }

            var errors = ModelState
                .Where(x => x.Value.Errors.Count > 0)
                .Select(x => new {x.Key, x.Value.Errors})
                .ToArray();

            if (ModelState.IsValid) {
                try {
                    await _eventManager.UpdateEventAsync(singleEvent);
                } catch (DbUpdateConcurrencyException) {
                    if (!EventExists(singleEvent.Id)) {
                        return NotFound();
                    } else {
                        throw;
                    }
                }

                return RedirectToAction("Index");
            }

            return PartialView(singleEvent);
        }

        // GET: Students/Delete/5
        public async Task<IActionResult> Delete(int? id) {
            if (id == null) {
                return NotFound();
            }

            var singleEvent = await _eventManager.GetEventAsync(id.Value);
            if (singleEvent == null) {
                return NotFound();
            }
            return PartialView(singleEvent);
        }

        // POST: Movies/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id) {
            await _eventManager.DeleteEventAsync(id);
            return RedirectToAction("Index");
        }

        private bool EventExists(long id) {
            return _eventManager.GetAllEventsAsync().Result.Any(e => e.Id == id);
        }

    }

}