using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

using Tracky.BL.Exceptions;
using Tracky.BL.Managers;
using Tracky.DAL;
using Tracky.DAL.Repository;
using Tracky.Model;
using Tracky.Web.Models;

namespace Tracky.Web.Controllers {

    public class UsersController : Controller {

        private readonly IUserManager _userManager;
        private readonly ILogger _logger;

        public UsersController(IUserManager userManager, ILoggerFactory loggerFactory) {
            _userManager = userManager;
            _logger = loggerFactory.CreateLogger<UsersController>();
        }

        /**
         * Login, Profile and registration actions
         */
        // GET: /users/login
        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> Login() {
            // Clear the existing external cookie to ensure a clean login process
            await _userManager.LogoutUserAsync();
            return View();
        }

        // POST: /users/login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model) {
            if (ModelState.IsValid) {
                // This doesn't count login failures towards account lockout
                // To enable password failures to trigger account lockout, set lockoutOnFailure: true
                var user = await _userManager.LoginUser(model.Email, model.Password);
                if (user == null) {
                    ModelState.AddModelError(string.Empty, "Invalid login attempt.");
                    return View(model);
                }
                _logger.LogInformation(1, "User logged in.");
                return RedirectToAction("Index");
            }
            // If we got this far, something failed, redisplay form
            return View(model);
        }


        // GET: /users/register
        [HttpGet]
        [AllowAnonymous]
        public IActionResult Register() {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterViewModel model) {
            if (ModelState.IsValid) {
                var userFromViewModel = new User {
                    Email = model.Email,
                    Password = model.Password,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    PhoneNumber = model.PhoneNumber
                };

                var savedUser = await _userManager.SaveUserAsync(userFromViewModel);
                if (savedUser == null) {
                    ModelState.AddModelError(string.Empty, "User creation failed!");
                    return View(model);
                }

                var loggedInUser = await _userManager.LoginUser(savedUser.Email, savedUser.Password);
                if (loggedInUser == null) {
                    ModelState.AddModelError(string.Empty, "User login failed!");
                    return View(model);
                }

                return RedirectToAction("Index");
            }
            // If we got this far, something failed, redisplay form
            return View(model);
        }

        [HttpGet]
        public IActionResult Logout() {
            return RedirectToAction("Login");
        }

        private void AddErrors(IdentityResult result) {
            foreach (var error in result.Errors) {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }

        // GET: /users/profile
        [HttpGet]
        public async Task<IActionResult> Profile() {
            var currentUserName = User.Identity.Name;
            if (currentUserName == null) {
                NotFound();
            }
            var user = await _userManager.GetUserByUsernameAsync(currentUserName);
            return View(user);
        }

        /**
         * CRUD actions
         */

        // GET: /users/index
        public async Task<IActionResult> Index() {
            return View(await _userManager.GetAllUsersAsync());
        }

        // GET: /users/5
        public async Task<IActionResult> Details(int id) {
            var user = await _userManager.GetUserAsync(id);
            if (user == null) {
                return NotFound();
            }
            return View(user);
        }

        public IActionResult Create() {
            return PartialView();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult>
            Create([Bind("Id,FirstName,LastName,Email, Password, PhoneNumber")] User user) {
            user.DateCreated = DateTime.UtcNow;
            user.DateModified = DateTime.UtcNow;

            if (ModelState.IsValid) {
                try {
                    await _userManager.SaveUserAsync(user);
                    return RedirectToAction("Index");
                } catch (UserException e) {
                    ModelState.AddModelError("Exception", e.Message);
                    return PartialView(user);
                }
            }
            return PartialView(user);
        }

        // GET: users/edit/5
        public async Task<IActionResult> Edit(int id) {
            var user = await _userManager.GetUserAsync(id);
            if (user == null) {
                return NotFound();
            }
            return PartialView(user);
        }

        // POST: Students/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id,
            [Bind("Id,FirstName,LastName,Email, Password, PhoneNumber")] User user) {
            if (id != user.Id) {
                return NotFound();
            }

            var persistedUser = await _userManager.GetUserAsync(id);
            if (persistedUser == null) {
                return NotFound();
            }

            if (ModelState.IsValid) {
                try {
                    persistedUser.Update(user);
                    var result = await _userManager.UpdateUserAsync(persistedUser);
                    return RedirectToAction("Index");
                } catch (DbUpdateException /* ex */) {
                    ModelState.AddModelError("", "Unable to save changes. " +
                                                 "Try again, and if the problem persists, " +
                                                 "see your system administrator.");
                }
            }
            return View(user);
        }

        // GET: users/delete/5
        public async Task<IActionResult> Delete(int? id) {
            if (id == null) {
                return NotFound();
            }

            var user = await _userManager.GetUserAsync(id.Value);
            if (user == null) {
                return NotFound();
            }
            return PartialView(user);
        }

        // POST: users/delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id) {
            var user = await _userManager.GetUserAsync(id);
            if (user == null) {
                return NotFound();
            }
            await _userManager.DeleteUserAsync(user);
            return RedirectToAction("Index");
        }

        private bool UserExists(int id) {
            return _userManager.GetAllUsersAsync().Result.Any(e => e.Id == id);
        }

    }

}