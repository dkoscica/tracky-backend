﻿using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.Configuration;

using Tracky.DAL;

namespace Tracky.DAL {

    public class DatabaseContextFactory : IDbContextFactory<DatabaseContext> {

        public DatabaseContext Create(DbContextFactoryOptions options) {
            var builder = new DbContextOptionsBuilder<DatabaseContext>();
            builder.UseSqlServer(DatabaseConstants.DatabaseUrl);
            return new DatabaseContext(builder.Options);
        }

    }

}