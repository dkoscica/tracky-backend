﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

using Tracky.DAL.Repository;
using Tracky.Model;

namespace Tracky.DAL {

    public class DatabaseContext : IdentityDbContext<User, UserRole, int> {

        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options) { }

        //  creates a DbSet property for each entity set. In Entity Framework terminology, 
        // an entity set typically corresponds to a database table, and an entity corresponds to a row in the table
        public DbSet<Event> Events { get; set; }
        public DbSet<UserEvent> UserEvents { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder) {

            foreach (var property in modelBuilder.Model.GetEntityTypes()
                .SelectMany(t => t.GetProperties())
                .Where(p => p.ClrType == typeof(decimal))) {
                property.Relational().ColumnType = "decimal(18, 8)";
            }

            modelBuilder.Entity<User>().ToTable("Users");
            modelBuilder.Entity<UserRole>().ToTable("UserRoles");

            modelBuilder.Entity<UserEvent>()
                .HasKey(userEvent => new {userEvent.EventId, userEvent.UserId});

//            modelBuilder.Entity<UserEvent>()
//                .HasOne(e => e.Event)
//                .WithMany(c => c.UserEvents)
//                .HasForeignKey(bc => bc.EventId);
//
//            modelBuilder.Entity<UserEvent>()
//                .HasOne(e => e.User)
//                .WithMany(c => c.UserEvents)
//                .HasForeignKey(bc => bc.UserId);

            base.OnModelCreating(modelBuilder);
        }

    }

}