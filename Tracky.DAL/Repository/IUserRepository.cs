﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

using Tracky.Model;

namespace Tracky.DAL.Repository {

    public interface IUserRepository {

        DatabaseContext DbContext();
        Task SaveUserAsync(User user);
        Task<List<User>> GetAllUsersAsync();
        Task<User> GetUserAsync(int id);
        Task DeleteUserAsync(int id);
        Task UpdateUserAsync(User user);

    }

}