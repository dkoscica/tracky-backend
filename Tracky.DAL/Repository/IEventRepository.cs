﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

using Tracky.Model;

namespace Tracky.DAL.Repository {

    public interface IEventRepository {

        Task SaveEvent(Event _event);
        Task<List<Event>> GetAllEvents();
        Task<Event> GetEvent(int id);
        Task DeleteEvent(int id);
        Task UpdateEvent(Event _event);
        Task <UserEvent> AddParticipantToEvent(User user, Event _event);
        Task <Event> GetEventWithAllParticipants(int id);

    }

}