﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

using Microsoft.EntityFrameworkCore;

using Tracky.Model;

namespace Tracky.DAL.Repository {

    public class UserRepository : IUserRepository {

        public DatabaseContext DbContext { get; }

        public UserRepository(DatabaseContext context) {
            DbContext = context;
        }

        DatabaseContext IUserRepository.DbContext() {
            return DbContext;
        }

        public async Task SaveUserAsync(User user) {
            user.DateCreated = DateTime.UtcNow;
            DbContext.Add(user);
            await DbContext.SaveChangesAsync();
        }

        public async Task<List<User>> GetAllUsersAsync() {
            return await DbContext.Set<User>().ToListAsync();
        }

        public async Task<User> GetUserAsync(int id) {
            return await DbContext.Set<User>().SingleOrDefaultAsync(m => m.Id == id);
        }

        public async Task DeleteUserAsync(int id) {
            var user = await GetUserAsync(id);
            DbContext.Set<User>().Remove(user);
            await DbContext.SaveChangesAsync();
        }

        public async Task UpdateUserAsync(User user) {
            throw new Exception("Not used!");
            //user.DateModified = DateTime.UtcNow;
            //DbContext.Update(user);
            //await DbContext.SaveChangesAsync();
        }


    }

}