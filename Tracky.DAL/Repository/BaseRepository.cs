﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

using Microsoft.EntityFrameworkCore;

using Tracky.Model;

namespace Tracky.DAL.Repository {

    public abstract class BaseRepository<T> where T : BaseEntity {

        protected DatabaseContext DbContext { get; }

        protected BaseRepository(DatabaseContext context) {
            DbContext = context;
        }

        protected async Task Save(T entity) {
            entity.DateCreated = DateTime.UtcNow;
            DbContext.Add(entity);
            await DbContext.SaveChangesAsync();
        }

        protected async Task<List<T>> GetAll() {
            return await DbContext.Set<T>().ToListAsync();
        }

        protected async Task<T> Get(int id) {
            return await DbContext.Set<T>().SingleOrDefaultAsync(m => m.Id == id);
        }

        protected async Task Delete(int id) {
            var entity = await Get(id);
            DbContext.Set<T>().Remove(entity);
            await DbContext.SaveChangesAsync();
        }

        protected async Task Update(T entity) {
            entity.DateModified = DateTime.UtcNow;
            DbContext.Update(entity);
            await DbContext.SaveChangesAsync();
        }

    }

}