﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

using Microsoft.EntityFrameworkCore;

using Tracky.Model;

namespace Tracky.DAL.Repository {

    public class EventRepository : BaseRepository<Event>, IEventRepository {

        public EventRepository(DatabaseContext context) : base(context) { }

        public async Task SaveEvent(Event e) {
            await Save(e);
        }

        public Task<List<Event>> GetAllEvents() {
            return DbContext.Set<Event>()
                .Include(e => e.UserEvents)
                .ToListAsync();
        }

        public Task<Event> GetEvent(int id) {
            return DbContext.Set<Event>()
                .Include(p => p.UserEvents)
                .FirstOrDefaultAsync(p => p.Id == id);
        }

        public async Task DeleteEvent(int id) {
            await Delete(id);
        }

        public async Task UpdateEvent(Event e) {
            await Update(e);
        }

        public async Task<UserEvent> AddParticipantToEvent(User user, Event _event) {
            var userEvent = new UserEvent {
                User = user,
                Event = _event
            };
            _event.UserEvents.Add(userEvent);
            //user.UserEvents.Add(userEvent);

            //DbContext.UserEvents.Add(userEvent);
            DbContext.SaveChanges();

            return userEvent;
        }

        public async Task<Event> GetEventWithAllParticipants(int id) {
            return await DbContext.Events
                .Include(e => e.UserEvents)
                .SingleOrDefaultAsync(x => x.Id == id);
        }

    }

}