﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

using Tracky.BL.Managers;
using Tracky.DAL;
using Tracky.DAL.Repository;
using Tracky.Model;

using Xunit;


namespace Tracky.BL.Data {

    public static class DbInitializer {

        private const int NumberOfUserMockEntries = 10;
        private const int NumberOfEventsMockEntries = 5;

        public static void Initialize(DatabaseContext context, IUserManager userManager,
            IEventManager eventManager) {
            context.Database.EnsureCreated();
            CreateSeeds(context, userManager, eventManager).Wait();
        }

        private static async Task CreateSeeds(DatabaseContext context, IUserManager userManager,
            IEventManager eventManager) {
            if (context.Events.Any()) {
                return; // DB has been seeded
            }

            var random = new Random();

            /**
             * Create mock users
             */
            for (int userIndex = 1; userIndex < NumberOfUserMockEntries; userIndex++) {
                var mockUser = new User {
                    FirstName = $"Dominik{userIndex}",
                    LastName = $"Košćica{userIndex}",
                    Email = $"dominik{userIndex}@gmail.com",
                    Password = $"password123{userIndex}",
                    PhoneNumber = $"091762162{userIndex}",
                    DateCreated = DateTime.UtcNow,
                    DateModified = DateTime.UtcNow,
                };
                await userManager.SaveUserAsync(mockUser);
            }

            var firstUser = await userManager.GetUserAsync(1);
            for (int eventIndex = 1; eventIndex < NumberOfEventsMockEntries; eventIndex++) {
                var mockEvent = new Event {
                    Name = $"Event{eventIndex}",
                    MaxNumberOfParticipants = random.Next(100),
                    SecretCode = random.Next(10000, 100000).ToString(),
                    LocationName = $"Location {random.Next(100)}",
                    Address = $"Address {random.Next(100)}",
                    Latitude = GenerateRandomLatitude(random),
                    Longitude = GenerateRandomLongitude(random),
                    StartDateTime = DateTime.UtcNow,
                    EndDateTime = DateTime.UtcNow,
                    DateCreated = DateTime.UtcNow,
                    DateModified = DateTime.UtcNow
                };
                mockEvent.UserEvents.Add(new UserEvent {
                    User = firstUser,
                    Event = mockEvent,
                });
                await eventManager.SaveEventAsync(mockEvent);
            }

            var firstEvent = await eventManager.GetEventAsync(1);
            var newMockUser = new User {
                FirstName = "Filip",
                LastName = $"Košćica",
                Email = $"filip@gmail.com",
                Password = $"password12332131",
                PhoneNumber = $"0000000000000000",
                DateCreated = DateTime.UtcNow,
                DateModified = DateTime.UtcNow,
            };

            firstEvent.UserEvents.Add(new UserEvent() {
                User = newMockUser,
                Event = firstEvent
            });
            await eventManager.UpdateEventAsync(firstEvent);

            var testFirstEvent = await eventManager.GetEventWithAllParticipants(1);
        }

        private static decimal GenerateRandomLatitude(Random random) {
            int lat = random.Next(516400146, 630304598);
            return Convert.ToDecimal(18d + lat / 1000000000d);
        }

        private static decimal GenerateRandomLongitude(Random random) {
            int lon = random.Next(224464416, 341194152);
            return Convert.ToDecimal(-72d - lon / 1000000000d);
        }

    }

}