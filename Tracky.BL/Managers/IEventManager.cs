﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

using Tracky.Model;

namespace Tracky.BL.Managers {

    public interface IEventManager {

        Task SaveEventAsync(Event _event);
        Task<List<Event>> GetAllEventsAsync();
        Task<Event> GetEventAsync(int id);
        Task DeleteEventAsync(int id);
        Task UpdateEventAsync(Event _event);
        Task<UserEvent> AddParticipantToEvent(User user, Event _event);
        Task<Event> GetEventWithAllParticipants(int id);
    }

}