﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Identity;

using Tracky.Model;

namespace Tracky.BL.Managers {

    public interface IUserManager {

        Task<User> LoginUser(string email, string password);
        Task LogoutUserAsync();
        Task<User> SaveUserAsync(User user);
        Task<List<User>> GetAllUsersAsync();
        Task<User> GetUserAsync(int id);
        Task<User> GetUserByUsernameAsync(string username);
        Task DeleteUserAsync(User user);
        Task<IdentityResult> UpdateUserAsync(User user);

    }

}