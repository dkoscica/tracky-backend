﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Tracky.DAL.Repository;
using Tracky.Model;

namespace Tracky.BL.Managers {

    public class EventManager : IEventManager {

        private readonly IEventRepository _repository;

        public EventManager(IEventRepository repository) {
            _repository = repository;
        }

        public async Task SaveEventAsync(Event _event) {
            await _repository.SaveEvent(_event);
        }

        public Task<List<Event>> GetAllEventsAsync() {
            return _repository.GetAllEvents();
        }

        public Task<Event> GetEventAsync(int id) {
            return _repository.GetEvent(id);
        }

        public Task DeleteEventAsync(int id) {
            return _repository.DeleteEvent(id);
        }

        public Task UpdateEventAsync(Event _event) {
            return _repository.UpdateEvent(_event);
        }

        public async Task<UserEvent> AddParticipantToEvent(User user, Event _event) {
            return await _repository.AddParticipantToEvent(user, _event);
        }

        public async Task<Event> GetEventWithAllParticipants(int id) {
            return await _repository.GetEventWithAllParticipants(id);
        }

    }

}