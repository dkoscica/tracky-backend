﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

using Tracky.BL.Exceptions;
using Tracky.DAL.Repository;
using Tracky.Model;

namespace Tracky.BL.Managers {

    public class UserManager : UserManager<User>, IUserManager {

        private readonly IUserRepository _repository;
        private readonly SignInManager<User> _signInManager;

        public UserManager(IUserStore<User> store,
            IOptions<IdentityOptions> optionsAccessor, IPasswordHasher<User> passwordHasher,
            IEnumerable<IUserValidator<User>> userValidators, IEnumerable<IPasswordValidator<User>> passwordValidators,
            ILookupNormalizer keyNormalizer, IdentityErrorDescriber errors, System.IServiceProvider services,
            ILogger<UserManager<User>> logger, IUserRepository repository, SignInManager<User> signInManager) : base(
            store, optionsAccessor,
            passwordHasher, userValidators, passwordValidators, keyNormalizer, errors, services, logger) {
            _repository = repository;
            _signInManager = signInManager;
        }

        public async Task<User> LoginUser(string email, string password) {
            var result = await _signInManager.PasswordSignInAsync(email, password, false, false);
            if (result.Succeeded) {
                return _repository.GetAllUsersAsync().Result
                    .FirstOrDefault(u => u.Email.Equals(email) && u.Password.Equals(password));
            }
            return null;
        }

        public async Task LogoutUserAsync() {
            await _signInManager.SignOutAsync();
        }

        public async Task<User> SaveUserAsync(User user) {

            var result = await CreateAsync(user, user.Password);
            if (result.Succeeded) {
                //await _signInManager.SignInAsync(user, isPersistent: false);
                //await _repository.SaveUserAsync(user);
                return user;
            }
            var errors = result.Errors;
            throw new UserException(string.Join($", ", errors.Select(m => $"{m.Description}")));
        }

        public Task<List<User>> GetAllUsersAsync() {
            return _repository.GetAllUsersAsync();
        }

        public Task<User> GetUserAsync(int id) {
            return _repository.GetUserAsync(id);
        }

        public async Task<User> GetUserByUsernameAsync(string username) {
            return await FindByNameAsync(username);
        }

        public Task DeleteUserAsync(User user) {
            return DeleteAsync(user);
        }

        public async Task<IdentityResult> UpdateUserAsync(User user) {
            return await UpdateAsync(user);
        }

    }

}