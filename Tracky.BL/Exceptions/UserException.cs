﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tracky.BL.Exceptions {

    public class UserException : Exception {

        public UserException(string message) : base(message) { }

    }

}